﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EdifyPractice.Models
{
    public class ClassExamTestR
    {
        public int Id { get; set; }


        [ForeignKey("FK_ClassExamTestR_Class")]
        public int ClassId { get; set; }

        [ForeignKey("FK_ClassExamTestR_ExamTest")]
        public int ExamTestId { get; set; }

        public virtual Class Class { get; set; }
        public virtual ExamTest ExamTest { get; set; }

        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<SubjectResultDetail> SubjectResultDetails { get; set; }
        public virtual Collection<Syllabus> Syllabi { get; set; }
    }
}