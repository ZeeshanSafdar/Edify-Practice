﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Parent 
    {

        public int Id { get; set; }

        [ForeignKey("FK_Student_Parent_User")]
        public int UserId { get; set; }

        public string Profession { get; set; }

        public string MonthlyIncome { get; set; }


        
        public virtual Collection<Student> Students { get; set; }
        public virtual Collection<Application> Applications { get; set; }
        public virtual Collection<Complaint> Complaints { get; set; }

    }
}
