﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Semantics;

namespace EdifyPractice.Models
{
    public class Event
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string EventName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }
        
        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        [ForeignKey("FK_Event_Branch")]
        public int BranchId { get; set; }

        [ForeignKey("FK_Event_School")]
        public int SchoolId { get; set; }


        public virtual Branch Branch { get; set; }
        public virtual School School { get; set; }
    }
}
