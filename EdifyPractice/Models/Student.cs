﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EdifyPractice.Models
{
    public class Student
    {

        public int Id { get; set; }

        [ForeignKey("FK_Student_User")]
        public int UserId { get; set; }

        [Required]
        public int RollNo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Admissiondate { get; set; }

        [Required]
        public double AdmissionFee{ get; set; }

        [ForeignKey("FK_Student_Parent")]
        public int ParentId { get; set; }

        [ForeignKey("FK_Student_Class")]
        public int ClassId { get; set; }

        [ForeignKey("FK_Student_Section")]
        public int SectionId { get; set; }

        public bool Status { get; set; }

        [ForeignKey("FK_Student_Fee")]
        public int FeeId { get; set; }

        public virtual Parent Parent { get; set; }
        public virtual Class Class { get; set; }
        public virtual Section Section { get; set; }
        public virtual Fee Fee { get; set; }

        
        public virtual Collection<StudentSectionR> StudentSectionRs { get; set; }
        public virtual Collection<StuAttendanceDetail> StuAttendancesDetails { get; set; }
        public virtual Collection<SubjectResultDetail> SubjectResultDetails { get; set; }
        public virtual Collection<StudentGatePass> StudentGatePasses { get; set; }
    }
}
