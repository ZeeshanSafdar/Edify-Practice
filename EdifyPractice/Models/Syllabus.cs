﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Syllabus
    {
        public int Id { get; set; }
        
        [ForeignKey("FK_Syllabus_ClassExamTestR")]
        public int ClassExamTestRId { get; set; }

        public virtual ClassExamTestR ClassExamTestR { get; set; }
        
        public virtual Collection<SyllabusDetail> SyllabusDetails { get; set; }
    }
}
