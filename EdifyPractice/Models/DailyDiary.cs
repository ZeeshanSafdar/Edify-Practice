﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class DailyDiary
    {
        public int Id { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        
        [ForeignKey("FK_DailyDiary_Section")]
        public int SectionId { get; set; }

        public virtual Section Section { get; set; }
        
        public virtual Collection<DailyDiaryDetail> DailyDiaryDetails { get; set; }
    }
}
