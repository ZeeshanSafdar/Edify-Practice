﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EdifyPractice.Models
{
    public class SubjectClassR
    {
        public int Id { get; set; }

        [ForeignKey("FK_SubjectClassR_Subject")]
        public int SubjectId { get; set; }

        [ForeignKey("FK_SubjectClassR_Class")]
        public int ClassId { get; set; }

        public virtual Subject Subject { get; set; }
        public virtual Class Class { get; set; }

        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<SyllabusDetail> SyllabusDetails { get; set; }
        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }
        public virtual Collection<LessonPlan> LessonPlans { get; set; }
        public virtual Collection<DatesheetDetail> DatesheetDetails { get; set; }
    }
}