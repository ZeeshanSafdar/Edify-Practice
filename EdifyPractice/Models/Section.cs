﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Section
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        [Required]
        public int TotalStudents { get; set; }

        [ForeignKey("FK_Section_Class")]
        public int ClassId { get; set; }


        public virtual Class Class { get; set; }
        
        public virtual Collection<SubjectSectionTeacherR> SubjectSectionTeacherRs { get; set; }
        public virtual Collection<SectionIncharge> SectionIncharges { get; set; }
        public virtual Collection<StudentSectionR> StudentSectionRs { get; set; }
        public virtual Collection<DailyDiary> DailyDiaries { get; set; }      

    }
}
