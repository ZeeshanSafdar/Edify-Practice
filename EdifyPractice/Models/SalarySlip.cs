﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SalarySlip
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [ForeignKey("FK_SalarySlip_Employee")]
        public int EmployeeId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public double Amount { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Collection<Pay> Pays { get; set; }

    }
}
