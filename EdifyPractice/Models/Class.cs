﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Class
    {
        public int Id { get; set; }

        [Required]

        [ForeignKey("FK_Class_Branch")]
        public int BranchId { get; set; }


        [Required]
        public string Name { get; set; }

        [Required]
        public int TotalSections { get; set; }


        public virtual Branch Branch { get; set; }

//        public virtual Collection<Subject> Subjects { get; set; }
        public virtual Collection<SubjectClassR> SubjectClassRs { get; set; }
        public virtual Collection<ClassExamTestR> ClassExamTestRs { get; set; }
        
        public virtual Collection<Section> Sections { get; set; }

        public virtual Collection<Fee> Fees { get; set; }


    }
}
