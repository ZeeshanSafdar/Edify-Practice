﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EdifyPractice.Models
{
    public class SubjectSectionTeacherR
    {
        public int Id { get; set; }

        [ForeignKey("FK_SubjectSectionTeacherR_Subject")]
        public int SubjectId { get; set; }

        [ForeignKey("FK_SubjectSectionTeacherR_Section")]
        public int SectionId { get; set; }

        [ForeignKey("FK_SubjectSectionTeacherR_Teacher")]
        public int TeacherId { get; set; }

        public virtual Subject Subject { get; set; }
        public virtual Section Section { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}