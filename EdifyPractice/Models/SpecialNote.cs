﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SpecialNote
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public string Description { get; set; }

        [ForeignKey("FK_SpecialNote_Teacher")]
        public int TeacherId { get; set; }

        [ForeignKey("FK_SpecialNote_StudentSectionR")]
        public int StudentSectionRId { get; set; }

        public virtual Teacher Teacher { get; set; }
        public virtual StudentSectionR StudentSectionR { get; set; }

    }
}
