﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class FeeCategory
    {
        public int Id { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public double Amount { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [Required]
        public string Month { get; set; }
        [Required]
        public string Status { get; set; }

    }
}
