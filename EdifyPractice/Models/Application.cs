﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Application
    {
        public int Id { get; set; }

        [Required]
        public string ApplicationSubject { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime ApplicationDate { get; set; }

        [ForeignKey("FK_Application_Parent")]
        public int ParentId { get; set; }

        [ForeignKey("FK_Application_Employee")]
        public int EmployeeId { get; set; }
        
        public virtual Parent Parent { get; set; }
        public virtual Employee Employee { get; set; }

    }
}
