﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class TimetableDetail
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }

        [ForeignKey("FK_TimetableDetail_SubjectClassR")]
        public int SubjectClassRId { get; set; }

        [ForeignKey("FK_TimetableDetail_Timetable")]
        public int TimetableId { get; set; }

        [ForeignKey("FK_TimetableDetail_TeacherId")]
        public int TeacherId { get; set; }

        public virtual SubjectClassR SubjectClassR { get; set; }
        public virtual Timetable Timetable { get; set; }
        public virtual Teacher Teacher { get; set; }


    }
}
