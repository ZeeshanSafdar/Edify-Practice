﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Department
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int TotalEmployee { get; set; }

        [ForeignKey("FK_Department_School")]
        public int SchoolId { get; set; }

        public virtual School School { get; set; }

        public virtual Collection<Employee> Employees { get; set; }
    }
}
