﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class DailyDiaryDetail
    {
        public int Id{ get; set; }

        [Required]
        public string Detail{ get; set; }

        public string Note { get; set; }

        [ForeignKey("FK_DailyDiaryDetail_Teacher")]
        public int TeacherId { get; set; }

        [ForeignKey("FK_DailyDiaryDetail_SubjectClassR")]
        public int SubjectClassRId { get; set; }

        [ForeignKey("FK_DailyDiaryDetail_DailyDiary")]
        public int DailyDiaryId { get; set; }

        public virtual Teacher Teacher { get; set; }
        public virtual SubjectClassR SubjectClassR { get; set; }
        public virtual DailyDiary DailyDiary { get; set; }
    }
}
