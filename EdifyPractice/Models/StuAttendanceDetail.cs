﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class StuAttendanceDetail
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Status { get; set; }

        [ForeignKey("FK_StuAttendanceDetail_Student")]
        public int StudentId { get; set; }

        [ForeignKey("FK_StuAttendanceDetail_StuAttendance")]
        public int StuAttendanceId { get; set; }

        public virtual Student Student { get; set; }

        public virtual StuAttendance StuAttendance { get; set; }
    }
}
