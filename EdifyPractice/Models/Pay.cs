﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Pay
    {
        public int Id { get; set; }
        [Required]
        public double Amount { get; set; }

        [ForeignKey("FK_Pay_SalarySlip")]
        public int SalarySlipId { get; set; }

        public virtual SalarySlip SalarySlip { get; set; }
        public virtual Collection<PayrollCategory> PayrollCategories { get; set; }
    }
}
