﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class LessonPlan
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime From { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime To { get; set; }

        [ForeignKey("FK_LessonPlan_Section")]
        public int SectionId { get; set; }

        [ForeignKey("FK_LessonPlan_Teacher")]
        public int TeacherId { get; set; }

        [ForeignKey("FK_LessonPlan_SubjectClassRId")]
        public int SubjectClassRId { get; set; }

        public virtual Section Section { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual SubjectClassR SubjectClassR { get; set; }

        public virtual Collection<LessonPlanDetail> LessonPlanDetails { get; set; }
    }
}
