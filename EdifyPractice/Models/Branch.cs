﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Branch
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address { get; set; }
        [ForeignKey("FK_Branch_School")]
        public int SchoolId { get; set; }

        public virtual School School { get; set; }

        public virtual Collection<User> Users { get; set; }
        public virtual Collection<Event> Events { get; set; }
        public virtual Collection<NewsUpdate> NewsUpdates { get; set; }
        public virtual Collection<Class> Classes { get; set; }


    }
}
