﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class School
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(11)]
        public string PhoneNum { get; set; }

        [Required]
        public string Logo { get; set; }

        [Required]
        [StringLength(150)]
        public string Address { get; set; }

        public virtual Collection<NewsUpdate> NewsUpdates { get; set; }
        public virtual Collection<AcademicYear> AcademicYears { get; set; }
        public virtual Collection<Branch> Branches { get; set; }
        public virtual Collection<Department> Departments { get; set; }

    }
}
