﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

namespace EdifyPractice.Models
{
    public class EmpAttendance
    {
        public int Id { get; set; }

        [ForeignKey("FK_EmpAttendance_Employee")]
        public int EmployeeId { get; set; }


        [Required]
        public string Status { get; set; }

        [Required]
        public DateTime AttendanceDate { get; set; }
         
        public virtual Employee Employee { get; set; }
        
    }
}
