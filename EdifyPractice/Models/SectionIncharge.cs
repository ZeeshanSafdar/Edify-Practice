﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SectionIncharge
    {
        public int Id { get; set; }

        [ForeignKey("FK_SectionIncharge_Teacher")]
        public int TeacherId { get; set; }
        

        [ForeignKey("FK_SectionIncharge_Section")]
        public int SectionId { get; set; }

        public virtual Teacher Teacher { get; set; }
        
        public virtual Section Section { get; set; }
    }
}
