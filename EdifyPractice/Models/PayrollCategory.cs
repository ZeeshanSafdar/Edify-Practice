﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class PayrollCategory
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public double Allowance { get; set; }

        [Required]
        public double Bonus { get; set; }

        [ForeignKey("FK_PayrollCategory_Pay")]
        public int PayId { get; set; }

        public virtual Pay Pay { get; set; }
    }
}
