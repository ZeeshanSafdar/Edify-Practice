﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class DatesheetDetail
    {
        public int Id { get; set; }
        
        [Required]
        public string Day { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [ForeignKey("FK_DatesheetDetail_SubjectClassRId")]
        public string SubjectClassRId { get; set; }

        public DateTime Time { get; set; }

        [ForeignKey("FK_DatesheetDetail_Datesheet")]
        public int DatesheetId { get; set; }

        public virtual Datesheet Datesheet { get; set; }
        public virtual SubjectClassR SubjectClassR { get; set; }
    }
}
