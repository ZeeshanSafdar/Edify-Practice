﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SubjectResult
    {
        public int Id { get; set; }
        [ForeignKey("FK_SubjectResult_Section")]
        public int SectionId { get; set; }
        [ForeignKey("FK_SubjectResult_ClassExamTestR")]
        public int ClassExamTestRId { get; set; }
        [ForeignKey("FK_SubjectResult_SubjectClassR")]
        public int SubjectClassRId { get; set; }
        [ForeignKey("FK_SubjectResult_Teacher")]
        public int TeacherId { get; set; }

        public virtual Section Section { get; set; }
        public virtual ClassExamTestR ClassExamTestR { get; set; }
        public virtual SubjectClassR SubjectClassR { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual Collection<SubjectResultDetail> SubjectResultDetails { get; set; }

    }
}
