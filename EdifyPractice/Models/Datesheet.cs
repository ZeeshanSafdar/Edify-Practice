﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Datesheet
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }       

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [Required]
        public string Note { get; set; }

        
        [ForeignKey("FK_Datesheet_ClassExamTestR")]
        public int ClassExamTestRId { get; set; }

        public virtual ClassExamTestR ClassExamTestR { get; set; }

        public virtual Collection<DatesheetDetail> DatesheetDetails { get; set; }
    }
}
