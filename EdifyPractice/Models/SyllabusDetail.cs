﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SyllabusDetail
    {
        public int Id { get; set; }

        [Required]
        public string Detail { get; set; }

        [ForeignKey("FK_SyllabusDetail_SubjectClassR")]
        public int SubjectClassRId { get; set; }

        [ForeignKey("FK_SyllabusDetail_Syllabus")]
        public int SyllabusId { get; set; }

        public virtual SubjectClassR SubjectClassR { get; set; }
        public virtual Syllabus Syllabus{ get; set; }

         
    }
}
