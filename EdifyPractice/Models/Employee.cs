﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Employee
    {
        public int Id { get; set; }

        [Required]
        public bool Status { get; set; }

        [ForeignKey("FK_Employee_Designation")]
        public int DesignationId { get; set; }
        [Required]
        public double CurrentSalary { get; set; }
        [Required]
        public double JoiningSalary { get; set; }
        [Required]
        public DateTime JoiningDate { get; set; }
        [Required]
        public string Qualification { get; set; }
        [Required]
        public int Experience { get; set; }
        [ForeignKey("FK_Employee_Department")]
        public int DepartmentId { get; set; }

        [ForeignKey("FK_Employee_User")]
        public int UserId { get; set; }

        public virtual Designation Designation { get; set; }
        public virtual Department Department { get; set; }
        public virtual User User { get; set; }

        public virtual Collection<SalarySlip> SalarySlips { get; set; }
        public virtual Collection<EmpAttendance> EmpAttendances { get; set; }
        public virtual Collection<Complaint> Complaints { get; set; }

    }
}
