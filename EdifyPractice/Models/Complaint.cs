﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Complaint
    {
        public int Id { get; set; }
        [ForeignKey("FK_Complaint_Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("FK_Complaint_Parent")]
        public int ParentId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Parent Parent { get; set; }

        [Required]
        public string ComplaintSubject { get; set; }

        public string ComplaintType { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime Date { get; set; }

    }
}
