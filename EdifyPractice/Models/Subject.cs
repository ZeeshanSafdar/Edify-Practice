﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Subject
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual Collection<SubjectClassR> SubjectClassRs { get; set; }
        public virtual Collection<SubjectSectionTeacherR> SubjectSectionTeacherRs { get; set; }
        
    }
}
