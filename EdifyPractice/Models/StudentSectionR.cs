﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EdifyPractice.Models
{
    public class StudentSectionR
    {
        public int Id { get; set; }

        [ForeignKey("FK_StudentSectionR_Student")]
        public int StudentId { get; set; }

        [ForeignKey("FK_StudentSectionR_Section")]
        public int SectionId { get; set; }

        public virtual Student Student { get; set; }

        public virtual Section Section { get; set; }
    }
}