﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class SubjectResultDetail
    {
       
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Remarks { get; set; }

        [Required]
        public int TotalMarks { get; set; }

        [Required]
        public int ObtainedMarks { get; set; }

        [ForeignKey("FK_SubjectResultDetail_Student")]
        public int StudentId { get; set; }

        [ForeignKey("FK_SubjectResultDetail_ClassExamTestR")]
        public int ClassExamTestRId { get; set; }

        [ForeignKey("FK_SubjectResultDetail_SubjectResult")]
        public int SubjectResultId { get; set; }

        public virtual Student Student{ get; set; }

        public virtual ClassExamTestR ClassExamTestR { get; set; }

        public virtual SubjectResult SubjectResult { get; set; }
    }
}
