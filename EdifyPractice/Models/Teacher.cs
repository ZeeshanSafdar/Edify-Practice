﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Teacher
    {
        public int Id { get; set; }

        [ForeignKey("FK_Teacher_User")]
        public int UserId { get; set; }

        public string Expertise { get; set; }
        public string TeachingHistory { get; set; }
        public string RecentInstitute { get; set; }

        public virtual Collection<SubjectSectionTeacherR> SubjectSectionTeacherRs { get; set; }
        public virtual Collection<Application> Applications { get; set; }
        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<LessonPlan> LessonPlans{ get; set; }
        public virtual Collection<DailyDiary> DailyDiaries { get; set; }
        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }


    }
}
