﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Fee
    {
        [Required]
        public int Id { get; set; }

//        [ForeignKey("FK_Fee_Student")]
//        public int StudentId { get; set; }

        [ForeignKey("FK_Fee_Class")]
        public int ClassId { get; set; }

        [Required]
        public double Amount { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public string Status { get; set; }

        
        public virtual Class Class { get; set; }

        public virtual Collection<Student> Students { get; set; }


    }
}
