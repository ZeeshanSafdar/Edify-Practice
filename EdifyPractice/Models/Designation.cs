﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Designation
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int TotalEmployees{ get; set; }

        public virtual Collection<Employee> Employees { get; set; }
    }
}
