﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class StuAttendance
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        

        [ForeignKey("FK_StuAttendance_Section")]
        public int SectionId { get; set; }

        public virtual Section Section { get; set; }
        

        public virtual Collection<StuAttendanceDetail> StuAttendanceDetails { get; set; }
    }
}
