﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class ExamTest
    {
        public int Id { get; set; }
        [Required]
        public string ExamName { get; set; }
        [Required]
        public string ExamType { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndDate { get; set; }

        public virtual Collection<ClassExamTestR> ClassExamTestR { get; set; }

    }
}
