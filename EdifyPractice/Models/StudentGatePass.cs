﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class StudentGatePass
    {
        public int Id { get; set; }
        [ForeignKey("FK_StudentGatePass_Student")]
        public int StudentId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateTime { get; set; }

        public virtual Student Student { get; set; }
    }
}
