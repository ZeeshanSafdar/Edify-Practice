﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPractice.Models
{
    public class Timetable
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Day { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }

        [Required]
        public bool Status { get; set; }

        [ForeignKey("FK_Timetable_Section")]
        public int SectionId { get; set; }

        public virtual Section Section { get; set; }

        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }
    }
}
